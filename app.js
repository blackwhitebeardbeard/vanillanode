var express = require('express');
var app = express();
var portNumber = 3000;
app.use(express.static('public'));

app.listen(portNumber, function () {
	  console.log('Example app listening on port ' + portNumber + '!');
});
